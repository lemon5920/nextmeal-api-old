const Koa = require("koa");
const Router = require("koa-router");
const BodyParser = require("koa-bodyparser");
const logger = require('koa-logger');
const serve = require('koa-static');
const cors = require('koa2-cors');
const jwt = require("./jwt");
const rootRoute = require('./routes/root');
const authRoute = require('./routes/auth');
const memberRoute = require('./routes/member');
const personalInfoRoute = require('./routes/personal-info');
const restaurantRoute = require('./routes/restaurant');
const restaurantManageRoute = require('./routes/restaurant-manage');
const tourRoute = require('./routes/tour');
const orderRoute = require('./routes/order');
const accountRoute = require('./routes/account');

const app = new Koa();
require("./mongo")(app);
app.use(BodyParser());
app.use(logger());
app.use(serve('upload-files'));
app.use(cors());

// ===================
//   Router for root
// ===================

const router = new Router();
router.use('/', rootRoute.routes());
app
    .use(router.routes())
    .use(router.allowedMethods());

// ===================
//   Router for auth
// ===================

const authRouter = new Router();
authRouter.use('/auth', authRoute.routes());
app
    .use(authRouter.routes())
    .use(authRouter.allowedMethods());

// =====================
//   Router for member
// =====================

const memberRouter = new Router();
memberRouter.use('/member', memberRoute.routes());
app
    .use(memberRouter.routes())
    .use(memberRouter.allowedMethods());

// ===================================
//   Router for personal information
//   Need Token
// ===================================

const personalInfoRouter = new Router();
personalInfoRouter.use(jwt.errorHandler()).use(jwt.jwt());
personalInfoRouter.use('/personal-info', personalInfoRoute.routes());
app
    .use(personalInfoRoute.routes())
    .use(personalInfoRoute.allowedMethods());

// =========================
//   Router for restaurant
// =========================

const restaurantRouter = new Router();
restaurantRouter.use('/restaurant', restaurantRoute.routes());
app
    .use(restaurantRoute.routes())
    .use(restaurantRoute.allowedMethods());

// ================================
//   Router for manage restaurant
//   Need Token
// ================================

const restaurantManageRouter = new Router();
restaurantManageRouter.use(jwt.errorHandler()).use(jwt.jwt());
restaurantManageRouter.use('/restaurant-manage', restaurantManageRoute.routes());
app
    .use(restaurantManageRoute.routes())
    .use(restaurantManageRoute.allowedMethods());

// ===================
//   Router for tour
//   Need Token
// ===================

const tourRouter = new Router();
tourRouter.use(jwt.errorHandler()).use(jwt.jwt());
tourRouter.use('/tour', tourRoute.routes());
app
    .use(tourRoute.routes())
    .use(tourRoute.allowedMethods());

// ====================
//   Router for order
//   Need Token
// ====================

const orderRouter = new Router();
orderRouter.use(jwt.errorHandler()).use(jwt.jwt());
orderRouter.use('/order', orderRoute.routes());
app
    .use(orderRoute.routes())
    .use(orderRoute.allowedMethods());

// ======================
//   Router for account
//   Need Token
// ======================

const accountRouter = new Router();
accountRouter.use(jwt.errorHandler()).use(jwt.jwt());
accountRouter.use('/account', accountRoute.routes());
app
    .use(accountRoute.routes())
    .use(accountRoute.allowedMethods());

// =================
//   Launch server
// =================

app.listen(Number(process.env.SERVER_PORT));

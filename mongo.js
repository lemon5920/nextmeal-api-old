require('dotenv').config();
const MongoClient = require('mongodb').MongoClient;
const MONGO_URL = `mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;

module.exports = function (app) {
    MongoClient.connect(MONGO_URL)
        .then((connection) => {
            app.restaurants = connection.collection("restaurants");
            app.members = connection.collection("members");
            console.log("Database connection established")
        })
        .catch((err) => console.error(err))
};

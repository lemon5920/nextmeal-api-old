const router = require('koa-router')();

router
    .get("/payable", async (ctx, next) => {
        ctx.body = "payable";
    })
    .get("/receivable", async (ctx, next) => {
        ctx.body = "receivable";
    });

module.exports = router;

require('dotenv').config();
const router = require('koa-router')();
const jwt = require("../jwt");

const SHA256 = require("crypto-js/sha256");

router
    // Login to get the token.
    .post("/login", async (ctx, next) => {
        const email = ctx.request.body.email + "";
        const password = SHA256(ctx.request.body.password + "").toString();
        const auth = {
            email,
            password
        };
        const userResult = await ctx.app.members.findOne(auth);
        if (!!userResult) {
            ctx.body = {
                token: jwt.issue({
                    _id: userResult._id,
                }),
                username: userResult.username
            }
        } else {
            ctx.status = 401;
            ctx.body = { msg: "帳號或密碼錯誤。" };
        }
    });

module.exports = router;

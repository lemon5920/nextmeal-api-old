require('dotenv').config();
const router = require('koa-router')();
const jwt = require("../jwt");

const SHA256 = require("crypto-js/sha256");

router
    // Sign up the user.
    .post("/sign-up", async (ctx, next) => {
        const username = ctx.request.body.username + "";
        const email = ctx.request.body.email + "";
        const password = ctx.request.body.password + "";
        const userExist = await ctx.app.members.findOne({ email });
        if (!!userExist) {
            ctx.status = 401;
            ctx.body = { msg: "信箱已被申請。" };
        } else {
            const insertData = {
                username,
                email,
                password: SHA256(password).toString(),
            }
            const insertResult = await ctx.app.members.insert(insertData);
            console.log(insertResult);
            if (insertResult.result.ok === 1) {
                ctx.status = 200;
                ctx.body = {
                    token: jwt.issue({
                        _id: insertResult.ops[0]._id,
                    }),
                    username: insertResult.ops[0].username
                }
            }
        }
    })
    // Verify the user exist or not.
    .post("/change-passwd-request", async (ctx, next) => {
        ctx.body = "change-passwd-request";
    })
    // Reset user's password.
    .put("/change-passwd/:token", async (ctx, next) => {
        ctx.body = "change-passwd";
    });

module.exports = router;

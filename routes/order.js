const router = require('koa-router')();

router
    .post("/:restaurantId", async (ctx, next) => {
        ctx.body = "點餐: " + ctx.params.restaurantId;
    });

module.exports = router;

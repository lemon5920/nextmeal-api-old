const router = require('koa-router')();
const jwtTokenProcess = require('../util/jwtTokenProcess');

router
    .get("/me", async (ctx, next) => {
        const token = ctx.request.header.authorization;
        const myId = jwtTokenProcess.tokenToObjectId(token);
        const myInfo = await ctx.app.members.findOne({ "_id": myId });
        ctx.body = {
            username: myInfo.username,
            email: myInfo.email,
        };
    })
    .put("/me", async (ctx, next) => {
        const token = ctx.request.header.authorization;
        const myId = jwtTokenProcess.tokenToObjectId(token);
        const documentQuery = { "_id": myId };
        const valuesToUpdate = ctx.request.body;
        const result = await ctx.app.members.update(documentQuery, { $set: valuesToUpdate });
        if (!!result.result.ok) {
            ctx.body = {
                "ok": true,
                "msg": "修改成功",
            }
        } else {
            ctx.status = 403;
            ctx.body = {
                "ok": true,
                "msg": "修改成功",
            }
        }
    });

module.exports = router;

const router = require('koa-router')();
const path = require('path');
const { uploadFile } = require('../util/upload');
const ObjectID = require("mongodb").ObjectID;

router
    .post("/", async (ctx, next) => {
        ctx.body = await ctx.app.restaurants.insert(ctx.request.body);
    })
    .post("/upload-menu-image", async (ctx, next) => {
        let result = { success: false }
        const serverFilePath = path.join(__dirname, '../upload-files')
        result = await uploadFile(ctx, {
            fileType: 'menu',
            path: serverFilePath
        })
        ctx.body = result
    })
    .put("/:restaurantId", async (ctx, next) => {
        const documentQuery = { "_id": ObjectID(ctx.params.restaurantId) };
        const valuesToUpdate = ctx.request.body;
        ctx.body = await ctx.app.restaurants.update(documentQuery, { $set: valuesToUpdate });
    })
    .delete("/:restaurantId", async (ctx, next) => {
        const documentQuery = { "_id": ObjectID(ctx.params.restaurantId) };
        ctx.body = await ctx.app.restaurants.deleteOne(documentQuery);
    });

module.exports = router;

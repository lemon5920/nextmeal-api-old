const router = require('koa-router')();
const ObjectID = require("mongodb").ObjectID;

router
    .get("/list", async (ctx, next) => {
        ctx.body = await ctx.app.restaurants.find().toArray();
    })
    .get("/:restaurantId", async (ctx, next) => {
        const _id = ObjectID(ctx.params.restaurantId);
        ctx.body = await ctx.app.restaurants.findOne({ _id });
    });

module.exports = router;

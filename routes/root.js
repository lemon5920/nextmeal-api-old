const router = require('koa-router')();
const path = require('path');
const { uploadFile } = require('../util/upload');

router
    .get("/", async (ctx, next) => {
        ctx.body = { message: "Welcome to NEXT MEAL." }
    });

module.exports = router;
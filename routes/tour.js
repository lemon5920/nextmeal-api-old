const router = require('koa-router')();

router
    .get("/all", async (ctx, next) => {
        ctx.body = "列出所有已開的團";
    })
    .get("/now", async (ctx, next) => {
        ctx.body = "列出現在正在開的團";
    })
    .get("/mine", async (ctx, next) => {
        ctx.body = "列出本人開的團";
    })
    .post("/launch/:restaurantId", async (ctx, next) => {
        ctx.body = "發起團購: " + ctx.params.restaurantId;
    })
    .post("/finish/:tourId", async (ctx, next) => {
        ctx.body = "結束一項團購: " + ctx.params.tourId;
    })
    .delete("/:tourId", async (ctx, next) => {
        ctx.body = "刪除一項團購: " + ctx.params.tourId;
    });

module.exports = router;

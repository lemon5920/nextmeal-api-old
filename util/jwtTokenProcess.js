require('dotenv').config({ path: '../' });
const aesKey = process.env.AES_KEY;
const ObjectID = require("mongodb").ObjectID;
const cryptoAES = require('crypto-js/aes');
const encUtf8 = require('crypto-js/enc-utf8');

/**
 * Encrypt user ID with AES.
 * @param {object} id - ID number of user.
 * @returns {String} Ciphertext of encryption performed on user ID.
 */
function ObjectIdToCiphertext (id) {
    return cryptoAES.encrypt(id.toString(), aesKey).toString();
}

/**
 * Decrypt JSON Web Tokens and return user ID.
 * @param {String} token - Ciphertext of JSON Web Tokens.
 * @returns {object} User ID.
 */
function tokenToObjectId (token) {
    const payloadCiphertext = token.split(".")[1];
    const payloadPlaintext = new Buffer(payloadCiphertext, 'base64').toString();
    const payloadJson = JSON.parse(payloadPlaintext);
    const id = payloadJson._id
    return ObjectID(id);
}

module.exports = {
    ObjectIdToCiphertext,
    tokenToObjectId
}

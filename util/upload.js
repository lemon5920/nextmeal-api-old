const inspect = require('util').inspect;
const path = require('path');
const fs = require('fs');
const Busboy = require('busboy');

/**
 * 同步創建目錄
 * @param  {string} dirname - 目錄絕對位置
 * @return {boolean}        - 創建目錄結果
 */
function mkdirsSync(dirname) {
    if (fs.existsSync(dirname)) {
        return true;
    } else {
        if (mkdirsSync(path.dirname(dirname))) {
            fs.mkdirSync(dirname);
            return true;
        }
    }
}

/**
 * 獲取上傳文件的副檔名
 * @param  {string} fileName    - 獲取上傳文件的副檔名
 * @return {string}             - 文件的副檔名
 */
function getSuffixName(fileName) {
    let nameList = fileName.split('.');
    return nameList[nameList.length - 1];
}

/**
 * 上傳文件
 * @param  {object} ctx     - koa 上下文
 * @param  {object} options - 文件上傳參數 fileType 文件類型， path 文件存放路徑
 * @return {promise}
 */
function uploadFile(ctx, options) {
    const req = ctx.req;
    const res = ctx.res;
    const busboy = new Busboy({ headers: req.headers });

    const fileType = options.fileType || 'common';
    const filePath = path.join(options.path, fileType);
    const mkdirResult = mkdirsSync(filePath);

    return new Promise((resolve, reject) => {
        console.log('上傳中');
        let result = {
            success: false,
            uploadedFileName: [],
            invalidFileName: [],
            message: '',
            // formData: {},
        }

        // 允許的檔案格式
        const validMIMEtype = ['image/jpeg', 'image/png', 'image/gif'];

        // 解析上傳之檔案
        busboy.on('file', function (fieldname, file, originalFileName, encoding, mimetype) {

            if (validMIMEtype.indexOf(mimetype) > -1) {
                const fileName = Math.random().toString(16).substr(2) + '.' + getSuffixName(originalFileName);
                const uploadFilePath = path.join(filePath, fileName);
                const saveTo = path.join(uploadFilePath);
                console.log(fileName);
                // 上傳檔案到指定路徑
                file.pipe(fs.createWriteStream(saveTo));

                // 上傳檔案結束
                file.on('end', function () {
                    result.uploadedFileName.push(fileName);
                })
            } else {
                file.resume();
                file.on('end', function () {
                    result.invalidFileName.push(originalFileName);
                })
            }
        })

        // 解析其他表單欄位
        // busboy.on('field', function (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
        //     console.log('formData: [' + fieldname + ']: value: ' + inspect(val));
        //     result.formData[fieldname] = inspect(val);
        // });

        // 上傳結束
        busboy.on('finish', function () {
            result.success = true;
            result.message = '上傳結束。';
            resolve(result);
        })

        // 錯誤處理
        busboy.on('error', function (err) {
            result.success = false;
            result.message = '上傳發生錯誤: ' + err;
            console.log(err);
            reject(result);
        })

        req.pipe(busboy);
    })

}

module.exports = {
    uploadFile
}